<?php
/**
 * Plugin Name: Chayka.Stronghold
 * Plugin URI: git@bitbucket.org:chaykaborya/chayka.stronghold.wpp.git
 * Description: WP Plugin designed to prevent WPScan and alike from scanning yor WP site
 * Version: 0.0.1
 * Author: Boris Mossounov <borix@tut.by>
 * Author URI: https://anotherguru.me
 * License: Proprietary
 */

require_once __DIR__.'/vendor/autoload.php';

if(!class_exists('Chayka\WP\Plugin')){
    if(\Chayka\Stronghold\StrongholdHelper::restoreEverything()){
        wp_redirect($_SERVER['REQUEST_URI'], 302);
    }

    add_action( 'admin_notices', function () {
?>
    <div class="error">
        <p>Chayka.Core plugin is required in order for Chayka.Stronghold to work properly</p>
    </div>
<?php
	});
}else{
    if(defined('WP_MOVED_THEMES_DIR')){
        register_theme_directory(WP_MOVED_THEMES_DIR);
    }
//    require_once dirname(__FILE__).'/Plugin.php';
	add_action('init', array('Chayka\Stronghold\Plugin', 'init'));
}
