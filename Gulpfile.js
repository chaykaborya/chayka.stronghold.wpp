/**
 * @var {Gulp} gulp
 */
var gulp = require('gulp');
var plumber = require('gulp-plumber');

var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');

var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var csslint = require('gulp-csslint');
var cssnano = require('gulp-cssnano');

var htmlmin = require('gulp-htmlmin');

var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var clean = require('gulp-clean');

var paths = {
    src: 'res/src',
    srcResJs: ['res/src/js/plugins.js','res/src/js/main.js'],
    srcResLess: ['res/src/css/**/*.less', '!res/src/css/**/.*.less'],
    srcResCss: ['res/src/css/**/*.css', '!res/src/css/normalize.css'],
    srcNgJs: ['res/src/ng/**/*.js'],
    srcNgLess: ['res/src/ng/**/*.less'],
    srcNgCss: ['res/src/ng/**/*.css'],
    srcNgHtml: ['res/src/ng/**/*.html'],
    srcImg: 'res/src/img/**/*.{png,gif,jpg}',
    dst: 'res/dist'
};

function handleError(err) {
    console.log(err.toString());
    this.emit('end');
}

gulp.task('clean', function(){
    return gulp.src([paths.dist], {read: false})
        .pipe(clean({force: true}));
});

/**
 * CSS
 */
gulp.task('css', function(){
    return gulp.src('res/src/css/normalize.css')
        .pipe(plumber(handleError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('res/dist/css'));
});

gulp.task('less:res', function(){
    return gulp.src(paths.srcResLess)
        .pipe(plumber(handleError))
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest('res/src/css'))
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(concat('build.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('res/dist/css'));
});

gulp.task('less:ng', function(){
    return gulp.src(paths.srcNgLess)
        .pipe(plumber(handleError))
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest('res/src/ng'))
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('res/dist/ng'));
});

gulp.task('less', ['less:res', 'less:ng']);

gulp.task('lint:css:res', function() {
    gulp.src(paths.srcResCss)
        .pipe(plumber(handleError))
        .pipe(csslint())
        .pipe(csslint.reporter())
    //.on('error', handleError)
    ;
});
gulp.task('lint:css:ng', function() {
    gulp.src(paths.srcNgCss)
        .pipe(plumber(handleError))
        .pipe(csslint())
        .pipe(csslint.reporter())
    //.on('error', handleError)
    ;
});

gulp.task('lint:css', ['lint:css:res', 'lint:css:ng']);

/**
 * JS
 */
gulp.task('js:res', function(){
    gulp.src(paths.srcResJs)
        .pipe(plumber(handleError))
        .pipe(sourcemaps.init())
        .pipe(uglify({
            mangle: false
        }))
        .pipe(concat('build.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('res/dist/js'));
});

gulp.task('js:ng', function(){
    gulp.src(paths.srcNgJs)
        .pipe(plumber(handleError))
        .pipe(sourcemaps.init())
        .pipe(uglify({
            mangle: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('res/dist/ng'));
});

gulp.task('js', ['js:res', 'js:ng']);

gulp.task('lint:js:res', function() {
    gulp.src(paths.srcResJs)
        .pipe(plumber(handleError))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
    //.on('error', handleError)
    ;
});

gulp.task('lint:js:ng', function() {
    gulp.src(paths.srcNgJs)
        .pipe(plumber(handleError))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
    //.on('error', handleError)
    ;
});

gulp.task('lint:js', ['lint:js:res', 'lint:js:ng']);

/**
 * Html
 */
gulp.task('html', function() {
    return gulp.src(paths.srcNgHtml)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('res/dist/ng'))
});

/**
 * Images
 */
gulp.task('img', function(){
    return gulp.src(paths.srcImg)
        .pipe(plumber(handleError))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.dst + '/img'));
});

gulp.task('lint', ['lint:js', 'lint:css']);

/**
 * Heads up, build does not include images optimization,
 * run it separately, if you need to.
 */
gulp.task('build', ['less', 'lint', 'js', 'css', 'html']);

gulp.task('watch', ['build'], function(){
    gulp.watch(paths.srcResLess, [
        'less:res',
        'lint:css:res'
    ]);
    gulp.watch(paths.srcNgLess, [
        'less:ng',
        'lint:css:ng'
    ]);
    gulp.watch(paths.srcResJs, [
        'lint:js:res',
        'js:res'
    ]);
    gulp.watch(paths.srcNgJs, [
        'lint:js:ng',
        'js:ng'
    ]);
    gulp.watch(paths.srcNgHtml, [
        'html'
    ]);
    gulp.watch(paths.srcImg, [
        'img'
    ]);
});

gulp.task('default', ['watch']);

