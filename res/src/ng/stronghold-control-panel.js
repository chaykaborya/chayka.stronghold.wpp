'use strict';

angular.module('stronghold-control-panel', ['chayka-ajax', 'chayka-spinners', 'chayka-modals', 'chayka-utils'])
    .directive('strongholdControlPanel', ['ajax', 'utils', function(ajax, utils){
        return {
            restrict: 'AE',
            templateUrl : utils.getResourceUrl('stronghold', 'ng/stronghold-control-panel.html'),
            controllerAs: 'ctrl',
            controller: function(){
                var ctrl = {

                    status: null,
                    
                    mode: 'simple',

                    isEverythingHidden: function(){
                        return  ctrl.status &&
                            ctrl.status.wpPluginsRootMoved &&
                            ctrl.status.wpPlugins['*'] === 'hidden' &&
                            ctrl.status.wpThemesRootMoved &&
                            ctrl.status.wpThemes['*'] === 'hidden';
                    },

                    isEverythingRestored: function(){
                        return  ctrl.status &&
                            !ctrl.status.wpPluginsRootMoved &&
                            ctrl.status.wpPlugins['*'] === 'restored' &&
                            !ctrl.status.wpThemesRootMoved &&
                            ctrl.status.wpThemes['*'] === 'restored';
                    },

                    calculateStatus: function (status) {
                        var allPluginsRestored = true,
                            allThemesRestored = true,
                            allPluginsHidden = true,
                            allThemesHidden = true,
                            name;
                        for(name in status.wpPlugins){
                            if(status.wpPlugins.hasOwnProperty(name) && name !== '*'){
                                allPluginsRestored = allPluginsRestored && !status.wpPlugins[name];
                                allPluginsHidden = allPluginsHidden && !!status.wpPlugins[name];
                            }
                        }
                        for(name in status.wpThemes){
                            if(status.wpThemes.hasOwnProperty(name) && name !== '*'){
                                allThemesRestored = allThemesRestored && !status.wpThemes[name];
                                allThemesHidden = allThemesHidden && !!status.wpThemes[name];
                            }
                        }
                        if(allPluginsHidden){
                            status.wpPlugins['*'] = 'hidden';
                        }else if(allPluginsRestored){
                            status.wpPlugins['*'] = 'restored';
                        }else{
                            status.wpPlugins['*'] = 'mixed';
                        }
                        if(allThemesHidden){
                            status.wpThemes['*'] = 'hidden';
                        }else if(allThemesRestored){
                            status.wpThemes['*'] = 'restored';
                        }else{
                            status.wpThemes['*'] = 'mixed';
                        }

                        return status;
                    },

                    setBestMode: function(){
                        if(ctrl.mode === 'simple' && !(ctrl.isEverythingHidden() || ctrl.isEverythingRestored())){
                            ctrl.mode = 'advanced';
                        }
                    },

                    onSuccess: function(data){
                        ctrl.calculateStatus(data.payload);
                        ctrl.status = data.payload;
                        ctrl.setBestMode();
                    },

                    checkStatus: function(){
                        ajax.get('/api/stronghold/status', {
                            success: ctrl.onSuccess
                        });
                    },

                    hidePluginsRoot: function(){
                        ajax.get('/api/stronghold/hide-wp-plugins-root', {
                            success: ctrl.onSuccess
                        });
                    },

                    restorePluginsRoot: function(){
                        ajax.get('/api/stronghold/restore-wp-plugins-root', {
                            success: ctrl.onSuccess
                        });
                    },

                    hideThemesRoot: function(){
                        ajax.get('/api/stronghold/hide-wp-themes-root', {
                            success: ctrl.onSuccess
                        });
                    },

                    restoreThemesRoot: function(){
                        ajax.get('/api/stronghold/restore-wp-themes-root', {
                            success: ctrl.onSuccess
                        });
                    },

                    hidePlugin: function(plugin){
                        ajax.post('/api/stronghold/hide-wp-plugin', {
                            plugin: plugin
                        }, {
                            success: ctrl.onSuccess
                        });
                    },

                    restorePlugin: function(plugin){
                        ajax.post('/api/stronghold/restore-wp-plugin', {
                            plugin: plugin
                        }, {
                            success: ctrl.onSuccess
                        });
                    },

                    hideTheme: function(theme){
                        ajax.post('/api/stronghold/hide-wp-theme', {
                            theme: theme
                        }, {
                            success: ctrl.onSuccess
                        });
                    },

                    restoreTheme: function(theme){
                        ajax.post('/api/stronghold/restore-wp-theme', {
                            theme: theme
                        }, {
                            success: ctrl.onSuccess
                        });
                    },

                    hideEverything: function(){
                        ajax.get('/api/stronghold/hide-everything', {
                            success: ctrl.onSuccess
                        });
                    },

                    restoreEverything: function(){
                        ajax.get('/api/stronghold/restore-everything', {
                            success: ctrl.onSuccess
                        });
                    }
                };

                ctrl.checkStatus();
                return ctrl;
            }
        };
    }])
;