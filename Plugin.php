<?php

namespace Chayka\Stronghold;

use Chayka\WP;

class Plugin extends WP\Plugin{

    /* chayka: constants */
    
    public static $instance = null;

    public static function init(){
        if(!static::$instance){
            static::$instance = $app = new self(__FILE__, array(
                'stronghold',
                /* chayka: init-controllers */
            ));
            $app->dbUpdate(array());
	        $app->addSupport_UriProcessing();
	        $app->addSupport_ConsolePages();
            
            StrongholdHelper::ensureEverythingIsHidden();

            /* chayka: init-addSupport */
        }
    }


    /**
     * Register your action hooks here using $this->addAction();
     */
    public function registerActions() {
        remove_action('wp_head', 'wp_generator');
        foreach ( array(
            'rss2_head',
            'commentsrss2_head',
            'rss_head',
            'rdf_header',
            'atom_head',
            'comments_atom_head',
            'opml_head',
            'app_head'
        ) as $action ) {
            remove_action( $action, 'the_generator' );
        }
        
        register_deactivation_hook(__DIR__ . '/Chayka.Stronghold.wpp.php', function(){
            StrongholdHelper::restoreEverything();
            wp_redirect($_SERVER['REQUEST_URI'], 302);
        });
    	/* chayka: registerActions */
    }

    /**
     * Register your action hooks here using $this->addFilter();
     */
    public function registerFilters() {
        /* chayka: registerFilters */
    }

    /**
     * Register scripts and styles here using $this->registerScript() and $this->registerStyle()
     *
     * @param bool $minimize
     */
    public function registerResources($minimize = false) {
        $this->registerBowerResources(true);

        $this->setResSrcDir('src/');
        $this->setResDistDir('dist/');

        $this->populateResUrl('stronghold');

        $this->registerNgScript('stronghold-control-panel', 'ng/stronghold-control-panel.js', ['chayka-ajax', 'chayka-modals', 'chayka-spinners', 'chayka-utils']);
        $this->registerNgStyle('stronghold-control-panel', 'ng/stronghold-control-panel.css', ['chayka-modals', 'chayka-spinners']);

		/* chayka: registerResources */
    }

    /**
     * Routes are to be added here via $this->addRoute();
     */
    public function registerRoutes() {
        $this->addRoute('default');
    }

    /**
     * Registering console pages
     */
    public function registerConsolePages(){
        $this->addConsolePage('Stronghold', 'update_core', 'stronghold', '/admin/stronghold', 'dashicons-shield', '75.40864573442377');

        /* chayka: registerConsolePages */
    }
}