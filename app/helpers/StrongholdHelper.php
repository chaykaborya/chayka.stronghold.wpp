<?php

namespace Chayka\Stronghold;
use Chayka\Helpers\FsHelper;
use Chayka\Helpers\HttpHeaderHelper;
use Chayka\Helpers\RandomizerHelper;
use Chayka\Helpers\Util;

/**
 * Main business logic here.
 * This helper is responsible for all WP obfuscation done by plugin.
 */
class StrongholdHelper{

    /**
     * Working version of wp-config.php
     *
     * @var string
     */
    private static $wpConfig = '';

    /**
     * Hashmap of active plugins [folder => phpPath]
     *
     * @var array
     */
    private static $activePlugins = [];

    /**
     * Hashmap of obfuscated plugins [pluginDir => obfuscatedDir]
     *
     * @var array
     */
    private static $hiddenPlugins = [];

    /**
     * Hashmap of obfuscated themes [themeDir => obfuscatedDir]
     *
     * @var array
     */
    private static $hiddenThemes = [];

    /**
     * Hashmap of obfuscated theme aliases [themeDir => aliasName]
     * 
     * @var array
     */
    private static $aliasedThemes = [];

    /**
     * Cached location of named plugin symlinks dir
     *
     * @var string
     */
    private static $namedPluginSymlinksDir = '';

    /**
     * Cached location of named themes symlinks dir
     *
     * @var string
     */
    private static $namedThemesSymlinksDir = '';

    /**
     * Generate random string
     *
     * @param int $length
     *
     * @return string
     */
    public static function generateRandomString($length = 10) {
//        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789ABCDEF';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Get wp-config.php file
     *
     * @return string
     */
    private static function wpConfigInstance(){
        if(!self::$wpConfig){
            self::$wpConfig = file_get_contents(ABSPATH.'wp-config.php');
        }
        return self::$wpConfig;
    }

    /**
     * Get wp config constant pattern
     *
     * @param $constant
     *
     * @return string
     */
    public static function wpConfigConstantPattern($constant){
        return '%define\([\'"]' . $constant . '[\'"][^;]+\)\s*;\s*\n(\n)?%m';
    }

    /**
     * Check if wp constant exists
     *
     * @param $constant
     *
     * @return int
     */
    public static function wpConfigConstantExists($constant){
        $pattern = self::wpConfigConstantPattern($constant);
        return preg_match($pattern, self::wpConfigInstance());
    }

    /**
     * Add or update wp-config.php constant
     *
     * @param $constant
     * @param $rawValue
     *
     * @return string
     */
    public static function wpConfigConstantUpdate($constant, $rawValue){
        if(self::wpConfigConstantExists($constant)){
            self::$wpConfig = preg_replace(self::wpConfigConstantPattern($constant), sprintf("define('%s', %s);\n\n", $constant, $rawValue), self::wpConfigInstance());
        }else{
            self::$wpConfig = preg_replace('%\/\*[^\n]+\*\/\s+require_once\(ABSPATH\s*\.\s*[\'"]wp-settings.php[\'"]\)\s*;%', sprintf("define('%s', %s);\n\n$0", $constant, $rawValue), self::wpConfigInstance());
        }
    }

    /**
     * Remove wp-config.php constant
     *
     * @param $constant
     */
    public static function wpConfigConstantRemove($constant){
        self::$wpConfig = preg_replace(self::wpConfigConstantPattern($constant), '', self::wpConfigInstance());
    }

    /**
     * Commit wp-config changes, save backup version.
     */
    public static function wpConfigCommit(){
        $wpConfig = self::wpConfigInstance();
        $tmpFn = ABSPATH . 'wp-config.' . date('Ymd.his') . '.php';
        rename(ABSPATH . 'wp-config.php', $tmpFn);
        file_put_contents(ABSPATH . 'wp-config.php', $wpConfig);
        if(file_exists(ABSPATH . 'wp-config.php')){
            unlink($tmpFn);
        }
    }

    /**
     * Check if plugin dir is moved
     *
     * @return bool
     */
    public static function isPluginsRootDirMoved(){
        return !is_dir(WP_CONTENT_DIR . '/plugins');
    }

    /**
     * Get current plugins root dir
     *
     * @return string
     */
    public static function getPluginsRootDir(){
        return defined('WP_PLUGIN_DIR') && self::isPluginsRootDirMoved() ?
            WP_PLUGIN_DIR : WP_CONTENT_DIR . '/plugins';
    }

    /**
     * Move /wp-content/plugins/ to /[rand]
     * Symlink /wp-plugins-rand/ to /[rand]
     * Patch wp-config.php
     *
     * @return bool need reload
     */
    public static function movePluginsRootDir(){
        if(!self::isPluginsRootDirMoved()){
            $symlink = 'wp-plugins.' . self::generateRandomString(8);
            $newDir = self::generateRandomString(8);
            rename(WP_CONTENT_DIR.'/plugins', ABSPATH.$newDir);
            symlink(ABSPATH.$newDir, ABSPATH.$symlink);
//            self::wpConfigConstantUpdate('PLUGINDIR', "dirname(__FILE__) . '/$newDir'");
            self::wpConfigConstantUpdate('WP_PLUGIN_DIR', "dirname(__FILE__) . '/$newDir'");
            self::wpConfigConstantUpdate('WP_PLUGIN_URL', "'/$newDir'");
            self::wpConfigCommit();

            return true;
        }

        return false;
    }

    /**
     * Return /wp-plugins-[rand] to /wp-content/plugins/
     * Remove symlink
     * Patch wp-config.php back
     *
     * @return bool need reload
     */
    public static function restorePluginsRootDir(){
        if(self::isPluginsRootDirMoved()){
            $symlinks = glob(ABSPATH.'wp-plugins.*');
            foreach($symlinks as $symlink){
                unlink($symlink);
            }
            rename(WP_PLUGIN_DIR, WP_CONTENT_DIR.'/plugins');
            self::wpConfigConstantRemove('PLUGINDIR');
            self::wpConfigConstantRemove('WP_PLUGIN_DIR');
            self::wpConfigConstantRemove('WP_PLUGIN_URL');
            self::wpConfigCommit();

            return true;
        }

        return false;
    }

    /**
     * Check if plugin dir is moved
     *
     * @return bool
     */
    public static function isThemesRootDirMoved(){
        return defined('WP_MOVED_THEMES_DIR') && is_dir(WP_MOVED_THEMES_DIR);
    }

    /**
     * Get current themes root dir
     *
     * @return string
     */
    public static function getThemesRootDir(){
        return defined('WP_MOVED_THEMES_DIR') && self::isThemesRootDirMoved() ?
            WP_MOVED_THEMES_DIR : WP_CONTENT_DIR . '/themes';
    }

    /**
     * Move /wp-content/themes/ to /[rand]
     * Symlink /wp-themes-rand/ to /[rand]
     * Patch wp-config.php
     *
     * @return bool need reload
     */
    public static function moveThemesRootDir(){
        if(!self::isThemesRootDirMoved()){
            $symlink = 'wp-themes.' . self::generateRandomString(8);
            $newDir = self::generateRandomString(8);
            rename(WP_CONTENT_DIR.'/themes', ABSPATH.$newDir);
            mkdir(WP_CONTENT_DIR.'/themes');
            symlink(ABSPATH.$newDir, ABSPATH.$symlink);
            self::wpConfigConstantUpdate('WP_MOVED_THEMES_DIR', "dirname(__FILE__) . '/$newDir'");
            self::wpConfigConstantUpdate('WP_MOVED_THEMES_URL', "'/$newDir'");
            self::wpConfigCommit();
            update_option('stylesheet_root', ABSPATH.$newDir);
            update_option('template_root', ABSPATH.$newDir);

            return true;
        }

        return false;
    }

    /**
     * Return /wp-themes-[rand] to /wp-content/themes/
     * Remove symlink
     * Patch wp-config.php back
     *
     * @return bool need reload
     */
    public static function restoreThemesRootDir(){
        if(self::isThemesRootDirMoved() && defined('WP_MOVED_THEMES_DIR')){
            $symlinks = glob(ABSPATH.'wp-themes.*');
            foreach($symlinks as $symlink){
                unlink($symlink);
            }
            rmdir(WP_CONTENT_DIR.'/themes');
            rename(WP_MOVED_THEMES_DIR, WP_CONTENT_DIR.'/themes');
            self::wpConfigConstantRemove('WP_MOVED_THEMES_DIR');
            self::wpConfigConstantRemove('WP_MOVED_THEMES_URL');
            self::wpConfigCommit();
            update_option('stylesheet_root', WP_CONTENT_DIR.'/themes');
            update_option('template_root', WP_CONTENT_DIR.'/themes');

            return true;
        }

        return false;
    }

    /**
     * Get hash-map of active plugins [dir => phpPath]
     *
     * @return array
     */
    public static function getActivePlugins(){
        if(empty(self::$activePlugins)){
            self::$activePlugins = [];
            $activePlugins = get_option('active_plugins');
            foreach($activePlugins as $phpPath){
                $dir = dirname($phpPath);
                $dir = strpos($phpPath, '/') ? $dir : $phpPath;
                self::$activePlugins[$dir] = $phpPath;
            }
        }

        return self::$activePlugins;
    }

    /**
     * Check if plugin is active
     *
     * @param string $pluginName
     *
     * @return bool
     */
    public static function isPluginActive($pluginName){
        $plugins = self::getActivePlugins();

        return isset($plugins[$pluginName]);
    }

    /**
     * Get named plugin symlinks directory
     *
     * @param bool $flushOld
     *
     * @return string
     */
    public static function getNamedPluginSymlinksDir($flushOld = false){
        if($flushOld){
            /**
             * If we are beginning from scratch and need to delete old stuff,
             * then create new one
             */
            $oldDirs = glob(ABSPATH.'wp-named-plugins.*');
            foreach($oldDirs as $dir){
                FsHelper::delete($dir);
            }
            self::$namedPluginSymlinksDir = ABSPATH.'wp-named-plugins.'.self::generateRandomString(8);
            mkdir(self::$namedPluginSymlinksDir);
        }else if(!self::$namedPluginSymlinksDir){
            /**
             * We need existing directory
             */
            $dirs = glob(ABSPATH.'wp-named-plugins.*');
            if(!empty($dirs)){
                self::$namedPluginSymlinksDir = reset($dirs);
            }else{
                self::$namedPluginSymlinksDir = ABSPATH.'wp-named-plugins.'.self::generateRandomString(8);
                mkdir(self::$namedPluginSymlinksDir);
            }
        }

        return self::$namedPluginSymlinksDir;
    }

    /**
     * Read directory of named plugin Symlinks,
     * remove obsolete ones, return array of [obfuscatedTarget => namedSymlink]
     *
     * @return array;
     */
    public static function getNamedPluginSymlinks(){
        $rootDir = self::getPluginsRootDir().'/';
        $symDir = self::getNamedPluginSymlinksDir().'/';
        $symlinks = FsHelper::readDir($symDir);
        $res = [];
        foreach($symlinks as $symlink){
            $isLink = is_link($symDir.$symlink);

            if($isLink){
                $readTarget = readlink($symDir.$symlink);
                $basename = basename($readTarget);
                if(!is_dir($readTarget) && !file_exists($readTarget)){
                    unlink($symDir.$symlink);
                    $newTarget = $rootDir.$basename;
                    if(is_dir($newTarget) || file_exists($newTarget)){
                        symlink($newTarget, $symDir.$symlink);
                        $res[$basename] = $symlink;
                    }
                }else{
                    $res[$basename] = $symlink;
                }
            }
        }

        return $res;
    }

    /**
     * Check if plugin is already hidden
     *
     * @param string $plugin
     *
     * @return bool
     */
    public static function isPluginHidden($plugin){
        $plugins = self::getNamedPluginSymlinks();

        return isset($plugins[$plugin])?$plugins[$plugin]:'';
    }

    /**
     * Get hidden status of all plugins
     *
     * @return array
     */
    public static function arePluginsHidden(){
        $pluginsRootDir = self::getPluginsRootDir();
        $plugins = FsHelper::readDir($pluginsRootDir, false);

        $hidden = ['*' => true] ;
        foreach($plugins as $plugin){
            if($plugin !== 'index.php'){
                $hidden[$plugin] = self::isPluginHidden($plugin);
                $hidden['*'] &= !!$hidden[$plugin];
            }
        }
        $hidden['*'] = !!$hidden['*'];

        return $hidden;
    }

    /**
     * Hide all plugins by obfuscating their names
     * Semi-obfuscated Symlink is created
     *
     * @return array
     */
    public static function hidePlugins(){
        /**
         * This useless line is needed to preload OptionHelper before file structure changes
         */
        OptionHelper::getOption('allPluginsAreHidden');

        self::getNamedPluginSymlinksDir(false);

        $pluginsRootDir = self::getPluginsRootDir();
        $plugins = FsHelper::readDir($pluginsRootDir, false);

        foreach($plugins as $plugin){
            self::hidePlugin($plugin, false);
        }

        $newActivePlugins = array_values(self::$activePlugins);
        update_option('active_plugins', $newActivePlugins);

        /**
         * If this flag set to true, plugin will automatically
         * hide all new & updated plugins
         */
        OptionHelper::setOption('allPluginsAreHidden', 1);

        return self::$hiddenPlugins;
    }

    /**
     * Hide specified plugin by obfuscating it's name.
     * Semi-obfuscated symlink is created
     *
     * @param $pluginName
     *
     * @param bool $updateActive
     *
     * @return string
     */
    public static function hidePlugin($pluginName, $updateActive = true){
        $pluginSymlinksDir = self::getNamedPluginSymlinksDir();
        $pluginsRootDir = self::getPluginsRootDir();
        if($pluginName === 'index.php'){
            copy($pluginsRootDir . '/index.php', $pluginSymlinksDir . '/index.php');
            return '';
        }


        /**
         * Checking if a plugin is a single php file
         */
        $isPhpFile      = preg_match('%\.(php|phtml)$%i', $pluginName);

        if(self::isPluginHidden($pluginName)){
            self::isPluginActive($pluginName);
            return '';
        }

        /**
         * Generating new plugin filesystem name
         */
        $newPluginName  = self::generateRandomString(8) . ($isPhpFile ? '.php' : '');
        while(is_dir($pluginsRootDir . '/' . $newPluginName)){
            $newPluginName = self::generateRandomString(8) . ($isPhpFile ? '.php' : '');
        }

        /**
         * Generating symlink to a plugin that is hard to guess but easy to read
         */
        $symlink = $isPhpFile ?
            FsHelper::hideExtension($pluginName) . '.' . self::generateRandomString(8) . '.php' :
            $pluginName . '.' . self::generateRandomString(8);

        $oldPath = $pluginsRootDir . '/' . $pluginName;
        $newPath = $pluginsRootDir . '/' . $newPluginName;
        $symPath = $pluginSymlinksDir . '/' . $symlink;

        /**
         * If plugin is active we need to update database data
         */
        if(self::isPluginActive($pluginName)){
            self::$activePlugins[ $pluginName ] = $isPhpFile ?
                $newPluginName :
                $newPluginName . '/' . basename(self::$activePlugins[ $pluginName ]);

            if($updateActive){
                $newActivePlugins = array_values(self::$activePlugins);
                update_option('active_plugins', $newActivePlugins);
            }
        }

        /**
         * Renaming plugin
         */
        rename($oldPath, $newPath);

        /**
         * Creating a symlink
         */
        symlink($newPath, $symPath);

        /**
         * Storing new name to hash-map of hidden plugins
         */
        self::$hiddenPlugins[ $pluginName ] = $newPluginName;
        return $newPluginName;
    }

    /**
     * Restore previously obfuscated plugins
     *
     * @return array
     */
    public static function restorePlugins(){
        $symlinksDir = self::getNamedPluginSymlinksDir(false);

        $pluginsRootDir = self::getPluginsRootDir();
        $plugins = FsHelper::readDir($pluginsRootDir, false);

        foreach($plugins as $plugin){
            self::restorePlugin($plugin, false);
        }

        $newActivePlugins = array_values(self::$activePlugins);
        update_option('active_plugins', $newActivePlugins);

        FsHelper::delete($symlinksDir);

        return self::$hiddenPlugins;
    }

    /**
     * Restore previously obfuscated plugin
     *
     * @param $pluginName
     *
     * @param bool $updateActive
     *
     * @return array
     */
    public static function restorePlugin($pluginName, $updateActive = true){
        /**
         * This useless line is needed to preload OptionHelper before file structure changes
         */
        OptionHelper::getOption('allPluginsAreHidden');

        $pluginSymlinksDir = self::getNamedPluginSymlinksDir();
        $pluginsRootDir = self::getPluginsRootDir();

        if($pluginName === 'index.php'){
            return '';
        }

        if(!self::isPluginHidden($pluginName)){
            self::isPluginActive($pluginName);
            return '';
        }

        /**
         * Checking if a plugin is a single php file
         */
        $isPhpFile      = preg_match('%\.(php|phtml)$%i', $pluginName);

        $symlinks = self::getNamedPluginSymlinks();

        $symlink = $symlinks[$pluginName];

        $restoredPluginName = $isPhpFile?
            preg_replace('%\.[\dA-F]{8}\.php$%', '.php', $symlink) :
            preg_replace('%\.[\dA-F]{8}$%', '', $symlink);

        $oldPath = $pluginsRootDir . '/' . $pluginName;
        $restoredPath = $pluginsRootDir . '/' . $restoredPluginName;
        $symPath = $pluginSymlinksDir . '/' . $symlink;

        /**
         * If plugin is active we need to update database data
         */
        if(self::isPluginActive($pluginName)){
            self::$activePlugins[ $pluginName ] = $isPhpFile ?
                $restoredPluginName :
                $restoredPluginName . '/' . basename(self::$activePlugins[ $pluginName ]);
            if($updateActive){
                $newActivePlugins = array_values(self::$activePlugins);
                update_option('active_plugins', $newActivePlugins);
            }
        }

        /**
         * Removing a symlink
         */
        unlink($symPath);

        /**
         * Renaming plugin
         */
        rename($oldPath, $restoredPath);

        /**
         * Storing restored name to hash-map of hidden plugins
         */
        self::$hiddenPlugins[ $pluginName ] = $restoredPluginName;

        /**
         * If this flag set to true, plugin will automatically
         * hide all new & updated plugins
         */
        OptionHelper::setOption('allPluginsAreHidden', 0);

        return $restoredPluginName;

    }

    /**
     * Ensure that all plugins are hidden if needed.
     * New and updated plugins are being hidden by this function.
     *
     * @return bool if reload is needed
     */
    public static function ensureAllPluginsAreHidden(){
        $needReload = false;
        $hiddenPlugins = self::arePluginsHidden();
        if(!$hiddenPlugins['*'] && OptionHelper::getOption('allPluginsAreHidden')){
            self::hidePlugins();
            $hiddenPlugins = self::arePluginsHidden();
            $needReload = $hiddenPlugins['*'];
        }

        return $needReload;
    }

    /**
     * Check if theme is active
     *
     * @param string $themeDirName
     *
     * @return bool
     */
    public static function isThemeActive($themeDirName){
        return get_stylesheet() === $themeDirName;
    }

    /**
     * Get named plugin symlinks directory
     *
     * @param bool $flushOld
     *
     * @return string
     */
    public static function getNamedThemeSymlinksDir($flushOld = false){
        if($flushOld){
            /**
             * If we are beginning from scratch and need to delete old stuff,
             * then create new one
             */
            $oldDirs = glob(ABSPATH.'wp-named-themes.*');
            foreach($oldDirs as $dir){
                FsHelper::delete($dir);
            }
            self::$namedThemesSymlinksDir = ABSPATH.'wp-named-themes.'.self::generateRandomString(8);
            mkdir(self::$namedThemesSymlinksDir);
        }else if(!self::$namedThemesSymlinksDir){
            /**
             * We need existing directory
             */
            $dirs = glob(ABSPATH.'wp-named-themes.*');
            if(!empty($dirs)){
                self::$namedThemesSymlinksDir = reset($dirs);
            }else{
                self::$namedThemesSymlinksDir = ABSPATH.'wp-named-themes.'.self::generateRandomString(8);
                mkdir(self::$namedThemesSymlinksDir);
            }
        }

        return self::$namedThemesSymlinksDir;
    }

    /**
     * Read directory of named plugin Symlinks,
     * remove obsolete ones, return array of [obfuscatedTarget => namedSymlink]
     *
     * @return array;
     */
    public static function getNamedThemeSymlinks(){
        $rootDir = self::getThemesRootDir().'/';
        $symDir = self::getNamedThemeSymlinksDir().'/';
        $symlinks = FsHelper::readDir($symDir);
        $res = [];
        foreach($symlinks as $symlink){
            $isLink = is_link($symDir.$symlink);

            if($isLink){
                $readTarget = readlink($symDir.$symlink);
                $basename = basename($readTarget);
                if(!is_dir($readTarget) && !file_exists($readTarget)){
                    unlink($symDir.$symlink);
                    $newTarget = $rootDir.$basename;
                    if(is_dir($newTarget) || file_exists($newTarget)){
                        symlink($newTarget, $symDir.$symlink);
                        $res[$basename] = $symlink;
                    }
                }else{
                    $res[$basename] = $symlink;
                }
            }
        }

        return $res;
    }

    /**
     * Check if plugin is already hidden
     *
     * @param string $theme
     *
     * @return bool
     */
    public static function isThemeHidden($theme){
        $themes = self::getNamedThemeSymlinks();

        return isset($themes[$theme])?$themes[$theme]:'';
    }

    /**
     * Get hidden status of all themes
     *
     * @return array
     */
    public static function areThemesHidden(){
        $themesRootDir = self::getThemesRootDir();
        $themes = FsHelper::readDir($themesRootDir, false);

        $hidden = ['*' => true] ;
        foreach($themes as $theme){
            if($theme !== 'index.php'){
                $hidden[ $theme ] = self::isThemeHidden($theme);
                $hidden['*'] &= !!$hidden[ $theme ];
            }
        }
        $hidden['*'] = !!$hidden['*'];

        return $hidden;
    }

    /**
     * Hide all themes by obfuscating their names
     * Semi-obfuscated Symlink is created
     *
     * @return array
     */
    public static function hideThemes(){
        /**
         * This useless line is needed to preload OptionHelper before file structure changes
         */
        OptionHelper::getOption('allThemesAreHidden');

        self::getNamedThemeSymlinksDir();

        $themesRootDir = self::getThemesRootDir();
        $themes = FsHelper::readDir($themesRootDir, false);

        foreach($themes as $themeDirName){
            self::hideTheme($themeDirName);
        }

        /**
         * If this flag set to true, plugin will automatically
         * hide all new & updated themes
         */
        OptionHelper::setOption('allThemesAreHidden', 1);

        return self::$hiddenThemes;
    }

    /**
     * Get and array of style.css headers
     *
     * @param $styleCssContent
     *
     * @return array
     */
    public static function readStyleCssHeader($styleCssContent){
        if(preg_match("%^\\s*\\/\\*(.*)\\*\\/%uUs", $styleCssContent, $m) &&
        preg_match_all("%^([^\\n\\a:]+)\\s*:\\s*([^\\n\\a]*)$%muUs", $m[1], $n, PREG_SET_ORDER)){
            $headers = [];
            foreach($n as $item){
                $headers[trim($item[1])] = trim($item[2]);
            }

            return $headers;
        }
        
        return [];
    }

    /**
     * Replaces style.css theme header with new provided values
     *
     * @param string $styleCssContent
     * @param array $headers
     *
     * @return string
     */
    public static function replaceStyleCssHeader($styleCssContent, $headers){

        $newHeader = "/*\n";
        foreach($headers as $key => $value){
            $newHeader .= sprintf("%s: %s\n", $key, $value);
        }
        $newHeader.= "*/";

        return preg_replace("%^\\s*\\/\\*(.*)\\*\\/%uUs", $newHeader, $styleCssContent);
    }

    /**
     * Hide specified plugin by obfuscating it's name.
     * Semi-obfuscated symlink is created
     *
     * @param $themeName
     *
     * @return string
     */
    public static function hideTheme($themeName){
        $themeSymlinksDir = self::getNamedThemeSymlinksDir(false);
        $themesRootDir = self::getThemesRootDir();
        if($themeName === 'index.php'){
            copy($themesRootDir . '/index.php', $themeSymlinksDir . '/index.php');
            return '';
        }

        if(!is_dir($themesRootDir.'/'.$themeName)){
            /**
             * Bypassing nonexistent theme folders, that can occur if we've already processed child-theme
             */
            return '';
        }
        
        if(self::isThemeHidden($themeName)){
            return '';
        }

        $newThemeName  = self::generateRandomString(8);
        while(is_dir($themesRootDir . '/' . $newThemeName)){
            $newThemeName = self::generateRandomString(8);
        }
        
        $newThemeHumanName = RandomizerHelper::getRandomCelebrity(self::$aliasedThemes);
        $newThemeHumanNameDashed = preg_replace('%\s+%u', '-', mb_strtolower($newThemeHumanName));

        $symlink = $themeName . '.' . self::generateRandomString(8);

        $oldPath = $themesRootDir . '/' . $themeName;
        $newPath = $themesRootDir . '/' . $newThemeName;
        $symPath = $themeSymlinksDir . '/' . $symlink;

        $styleCssFn = $oldPath.'/style.css';

        $styleCss = file_get_contents($styleCssFn);

        $headers = self::readStyleCssHeader($styleCss);

        $headers['Theme Name'] = $newThemeHumanName;
        $headers['Theme URI'] = 'https://' . $newThemeHumanNameDashed . '.com';
        $headers['Author'] = $newThemeHumanName;
        $headers['Author URI'] = 'https://' . $newThemeHumanNameDashed . '.com';
        $headers['Description'] = '';
        $headers['License'] = 'GNU General Public License v2 or later';
        $headers['License URI'] = 'http://www.gnu.org/licenses/gpl-2.0.html';

        if(isset($headers['Template'])){
            $parent = self::hideTheme($headers['Template']);
            $headers['Template'] = $parent;
        }

        if(isset($headers['Text Domain'])){
            $files = glob($oldPath.'/languages/'.$headers['Text Domain'].'.*');
            foreach($files as $file){
                $ext = FsHelper::getExtension($file);
//                symlink(basename($file), $oldPath.'/languages/'.$newThemeHumanNameDashed . '/' . $ext);
                rename($file, $oldPath.'/languages/'.$newThemeHumanNameDashed . '.' . $ext);
            }
            $headers['Text Domain'] = $newThemeHumanNameDashed;
        }

        $allowedHeaders = [
            'Theme Name', 'Theme URI',
            'Author', 'Author URI',
            'Description',
            'License', 'License URI',
            'Template', 'Text Domain'
        ];
        foreach($headers as $key => $value){
            if(!in_array($key, $allowedHeaders)){
                unset($headers[$key]);
            }
        }

        $styleCss = self::replaceStyleCssHeader($styleCss, $headers);

        rename($oldPath . '/style.css', $oldPath . '/style.original.' . self::generateRandomString(4) . '.css');
        file_put_contents($oldPath . '/style.css', $styleCss);

        if(self::isThemeActive($themeName)){
            update_option('stylesheet', $newThemeName);
            update_option('template', $newThemeName);
        }
        rename($oldPath, $newPath);
        symlink($newPath, $symPath);

        self::$hiddenThemes[ $themeName ] = $newThemeName;
        return $newThemeName;
    }

    /**
     * Restore all themes that were previously obfuscated
     *
     * @return array
     */
    public static function restoreThemes(){
        $symlinksDir = self::getNamedThemeSymlinksDir(false);

        $themesRootDir = self::getThemesRootDir();
        $themes = FsHelper::readDir($themesRootDir, false);

        foreach($themes as $themeDirName){
            self::restoreTheme($themeDirName);
        }

        FsHelper::delete($symlinksDir);

        return self::$hiddenThemes;
    }

    /**
     * Restore theme that was previously obfuscated
     *
     * @param $themeName
     *
     * @return string
     */
    public static function restoreTheme($themeName){
        /**
         * This useless line is needed to preload OptionHelper before file structure changes
         */
        OptionHelper::getOption('allThemesAreHidden');

        $themeSymlinksDir = self::getNamedPluginSymlinksDir(false);
        $themesRootDir = self::getThemesRootDir();
        if($themeName === 'index.php'){
            return '';
        }

        if(!is_dir($themesRootDir.'/'.$themeName)){
            /**
             * Bypassing nonexistent theme folders, that can occur if we've already processed child-theme
             */
            return '';
        }

        if(!self::isThemeHidden($themeName)){
            return '';
        }

        $symlinks = self::getNamedThemeSymlinks();

        $symlink = $symlinks[$themeName];

        $restoredThemeName = preg_replace('%\.[\dA-F]{8}$%', '', $symlink);

        $oldPath = $themesRootDir . '/' . $themeName;
        $restoredPath = $themesRootDir . '/' . $restoredThemeName;
        $symPath = $themeSymlinksDir . '/' . $symlink;

        $styleCssFn = $oldPath.'/style.css';

        $styleCss = file_get_contents($styleCssFn);

        $headers = self::readStyleCssHeader($styleCss);

        if(isset($headers['Template'])){
            $parent = self::restoreTheme($headers['Template']);
            $headers['Template'] = $parent;
        }

        $textDomain = Util::getItem($headers, 'Text Domain');

        $styleCss = self::replaceStyleCssHeader($styleCss, $headers);

        $files = glob($oldPath . '/style.original.*.css');
        $originalStyleCssFn = !empty($files)?reset($files):'';

        if($originalStyleCssFn){
            FsHelper::delete($styleCssFn);
            rename($originalStyleCssFn, $styleCssFn);
            $styleCss = file_get_contents($styleCssFn);
            $headers = self::readStyleCssHeader($styleCss);
            if(isset($headers['Text Domain'])){
                $files = glob($oldPath.'/languages/'.$textDomain.'.*');
                foreach($files as $file){
                    $ext = FsHelper::getExtension($file);
                    rename($file, $oldPath.'/languages/'.$headers['Text Domain'] . '.' . $ext);

//                    if(is_link($file)){
//                        $original = basename(readlink($file));
//                        $headers['Text Domain'] = FsHelper::hideExtension($original);
//                        unlink($file);
//                    }
                }
            }

        }else{
            file_put_contents($oldPath . '/style.css', $styleCss);
        }

        if(self::isThemeActive($themeName)){
            update_option('stylesheet', $restoredThemeName);
            update_option('template', $restoredThemeName);
        }

        unlink($symPath);
        rename($oldPath, $restoredPath);

        self::$hiddenThemes[ $themeName ] = $restoredThemeName;

        /**
         * If this flag set to true, plugin will automatically
         * hide all new & updated themes
         */
        OptionHelper::setOption('allThemesAreHidden', 0);

        return $restoredThemeName;
    }

    /**
     * Ensure that all themes are hidden if needed.
     * New and updated themes are being hidden by this function.
     *
     * @return bool if reload is needed
     */
    public static function ensureAllThemesAreHidden(){
        $needReload = false;
        $hiddenThemes = self::areThemesHidden();
        if(!$hiddenThemes['*'] && OptionHelper::getOption('allThemesAreHidden')){
            self::hideThemes();
            $hiddenThemes = self::areThemesHidden();
            $needReload = $hiddenThemes['*'];
        }

        return $needReload;
    }

    /**
     * This function ensures that plugins' and themes' folders are obfuscated.
     * Does not obfuscate root plugins and themes folders.
     */
    public static function ensureEverythingIsHidden(){

        $needReload1 = self::ensureAllPluginsAreHidden();
        $needReload2 = self::ensureAllThemesAreHidden();

        if($needReload1 || $needReload2){
            wp_redirect($_SERVER['REQUEST_URI'], 302);
        }
    }

    /**
     * Hide everything:
     *
     * - Obfuscate each plugin folder
     * - Obfuscate root plugin folder
     * - Obfuscate each theme folder
     * - Obfuscate root theme folder
     *
     * Redirect reload should be performed after this function call,
     * otherwise request will crash.
     */
    public static function hideEverything(){

        /**
         * Preload RandomizerHelper
         */
        RandomizerHelper::getRandomCelebrity();

        self::hidePlugins();
        self::hideThemes();
        self::movePluginsRootDir();
        self::moveThemesRootDir();
    }

    /**
     * Hide everything:
     *
     * - Obfuscate each plugin folder
     * - Obfuscate root plugin folder
     * - Obfuscate each theme folder
     * - Obfuscate root theme folder
     *
     * Redirect reload should be performed after this function call,
     * otherwise request will crash.
     *
     * @return bool need reload
     */
    public static function restoreEverything(){

        /**
         * Preload RandomizerHelper
         */
        RandomizerHelper::getRandomCelebrity();

        self::restorePlugins();
        self::restoreThemes();
        $needReload1 = self::restorePluginsRootDir();
        $needReload2 = self::restoreThemesRootDir();

        return $needReload1 || $needReload2;
    }

}