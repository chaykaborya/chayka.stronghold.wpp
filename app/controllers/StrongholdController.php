<?php

namespace Chayka\Stronghold;

use Chayka\Helpers\FsHelper;
use Chayka\Helpers\InputHelper;
use Chayka\Helpers\RandomizerHelper;
use Chayka\WP\Helpers\AclHelper;
use Chayka\WP\MVC\Controller;
use Chayka\WP\Helpers\JsonHelper;

/**
 * Class StrongholdController is responsible for all actions required
 * to control Stronghold functionality
 *
 * @package Chayka\Stronghold
 */
class StrongholdController extends Controller{

    public function init(){
        InputHelper::captureInput();
        AclHelper::apiPermissionRequired();
    }

    /**
     * Get info on stronghold functionality status
     */
    public function statusAction(){

        $plugins = StrongholdHelper::arePluginsHidden();
        foreach($plugins as $key => $val){
            if($val && $key!=='*'){
                $plugins[$key] = preg_replace('%\.[\w\d]+(\.php)?$%u', '', $val);
            }
        }

        $themes = StrongholdHelper::areThemesHidden();
        foreach($themes as $key => $val){
            if($val && $key!=='*'){
                $themes[$key] = preg_replace('%\.[\w\d]+$%u', '', $val);
            }
        }

        $status = [
            'wpPluginsRootDir' => str_replace(ABSPATH, '', StrongholdHelper::getPluginsRootDir()),
            'wpPluginsRootMoved' => StrongholdHelper::isPluginsRootDirMoved(),
            'wpPlugins' => $plugins,
            'wpThemesRootDir' => str_replace(ABSPATH, '', StrongholdHelper::getThemesRootDir()),
            'wpThemesRootMoved' => StrongholdHelper::isThemesRootDirMoved(),
            'wpThemes' => $themes,
        ];

        JsonHelper::respond($status);
    }

    /**
     * Hide (move) /wp-content/plugins to an obfuscated folder
     */
    public function hideWpPluginsRootAction(){
        StrongholdHelper::movePluginsRootDir();
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Restore (move back) /wp-content/plugins from an obfuscated folder
     */
    public function restoreWpPluginsRootAction(){
        StrongholdHelper::restorePluginsRootDir();
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Hide (move) /wp-content/themes to an obfuscated folder
     */
    public function hideWpThemesRootAction(){
        StrongholdHelper::moveThemesRootDir();
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Restore (move back) /wp-content/themes from an obfuscated folder
     */
    public function restoreWpThemesRootAction(){
        StrongholdHelper::restoreThemesRootDir();
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Hide (move) specified plugin or all of them to an obfuscated folder
     */
    public function hideWpPluginAction(){
        $plugin = InputHelper::getParam('plugin', '*');
        if('*' === $plugin){
            StrongholdHelper::hidePlugins();
        } else {
            StrongholdHelper::hidePlugin($plugin);
        }
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Restore (move back) specified plugin or all of them from an obfuscated folder
     */
    public function restoreWpPluginAction(){
        $plugin = InputHelper::getParam('plugin', '*');
        if('*' === $plugin){
            StrongholdHelper::restorePlugins();
        }else{
            StrongholdHelper::restorePlugin($plugin);
        }
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Hide (move) specified theme or all of them from an obfuscated folder
     */
    public function hideWpThemeAction(){
        $theme = InputHelper::getParam('theme', '*');
        if('*' === $theme){
            StrongholdHelper::hideThemes();
        }else{
            StrongholdHelper::hideTheme($theme);
        }
        wp_redirect('/api/stronghold/status/');
    }

    /**
     * Restore (move back) specified theme or all of them from an obfuscated folder
     */
    public function restoreWpThemeAction(){
        $theme = InputHelper::getParam('theme', '*');
        if('*' === $theme){
            StrongholdHelper::restoreThemes();
        }else{
            StrongholdHelper::restoreTheme($theme);
        }
        wp_redirect('/api/stronghold/status/');
    }

    public function hideEverythingAction(){
        StrongholdHelper::hideEverything();
        wp_redirect('/api/stronghold/status/');
    }

    public function restoreEverythingAction(){
        StrongholdHelper::restoreEverything();
        wp_redirect('/api/stronghold/status/');
    }

}