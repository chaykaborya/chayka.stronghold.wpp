<?php

namespace Chayka\Stronghold;

use Chayka\WP\MVC\Controller;

class AdminController extends Controller{

    public function init(){
//        $this->enqueueNgScriptStyle('chayka-options-form');
    }

    public function strongholdAction(){
        $this->enqueueNgScriptStyle('stronghold-control-panel');
    }
}